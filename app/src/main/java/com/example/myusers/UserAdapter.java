package com.example.myusers;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myusers.models.user_name;

import java.util.ArrayList;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private ArrayList<user_name> user_number;

    public UserAdapter(ArrayList<user_name> user_number){
        this.user_number = user_number;

    }
    public class UserViewHolder extends RecyclerView.ViewHolder {

        private TextView nametxt,numbertxt;
        private TextView jobtxt,sumtxt;

        public UserViewHolder(@NonNull View itemView) {
            super(itemView);

            nametxt = itemView.findViewById(R.id.user_names);
            numbertxt = itemView.findViewById(R.id.user_number);
            jobtxt = itemView.findViewById(R.id.user_job);
            sumtxt = itemView.findViewById(R.id.user_sum);

        }
    }
    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_number, parent,false);
        return new UserViewHolder(item);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {

        String name = user_number.get(position).getName();
        String number = user_number.get(position).getNumber();
        String job = user_number.get(position).getJob();
        String summa = user_number.get(position).getSumma();

        holder.nametxt.setText(name);
        holder.numbertxt.setText(number);
        holder.sumtxt.setText(summa);
        holder.jobtxt.setText(job);

    }

    @Override
    public int getItemCount() {
        return user_number.size();
    }


}
